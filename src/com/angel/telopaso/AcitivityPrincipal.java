package com.angel.telopaso;

import java.util.regex.Pattern;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.angel.telopaso.dao.TeloPasoDaoWrapper;
import com.angel.telopaso.fragments.factory.FragmentFactory;
import com.angel.telopaso.layout.objects.User;
import com.angel.telopaso.listeners.DrawItemClickListener;
import com.angel.telopaso.service.ServiceSender;
import com.angel.telopaso.utils.Constantes;

/**
 * Actividad inicial que se encargad del inicio de las configuraciones de la
 * aplicación Comprueba la conectividad del dispositivo, realiza el auto
 * registro si fuera necesario y carga la interfaz Draw Navigator de la
 * aplicacion
 * */
public class AcitivityPrincipal extends Activity {

	private CharSequence titulo;
	private CharSequence tituloDrawer;
	private DrawerLayout drawerLayout;
	private ActionBarDrawerToggle actionBar;
	private ListView listaOpciones;
	private String[] opciones;
	private FragmentFactory fragmentFactory;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		if (!isConnected) {
			Toast.makeText(this, "Debes conectarte a internet para poder usar la App", Toast.LENGTH_LONG).show();
		}

		String userName = getAccountName();
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);
		TeloPasoDaoWrapper wrapper = new TeloPasoDaoWrapper(this);
		if (!wrapper.existsUser()) {
			User user = new User();
			user.setEmailUser(userName);
			wrapper.createUser(user);
		}
		SharedPreferences.Editor editor = this.getPreferences(MODE_PRIVATE).edit();
		editor.putString("userName", userName);
		editor.commit();

		Intent intent = new Intent(this, ServiceSender.class);
		intent.putExtra("userName", userName);
		startService(intent);

		fragmentFactory = FragmentFactory.getInstance(getFragmentManager(), this);
		setContentView(R.layout.activity_principal);
		titulo = tituloDrawer = getTitle();
		opciones = getResources().getStringArray(R.array.opciones);
		drawerLayout = (DrawerLayout) findViewById(R.id.drawLayout);
		listaOpciones = (ListView) findViewById(R.id.opciones);
		listaOpciones.setAdapter(new ArrayAdapter<String>(this, R.layout.layout_menu_lateral, opciones));
		listaOpciones.setOnItemClickListener(new DrawItemClickListener(fragmentFactory, this));
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		actionBar = new ActionBarDrawerToggle(this, drawerLayout, R.drawable.ic_drawer, R.string.drawer_open,
				R.string.drawer_close) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(titulo);
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(tituloDrawer);
				invalidateOptionsMenu();
			}
		};
		drawerLayout.setDrawerListener(actionBar);

		if (savedInstanceState == null) {
			fragmentFactory.selectFragment(Constantes.FRAGMENT_LIST_NOTIFICACIONES);
		}

	}

	@Override
	public void setTitle(CharSequence title) {
		titulo = title;
		getActionBar().setTitle(titulo);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		actionBar.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		actionBar.onConfigurationChanged(newConfig);
	}

	/**
	 * Metodo que obtiene la cuenta de correo del dispositvo para utilizarla
	 * como nombre de usuario
	 * */
	private String getAccountName() {
		Pattern emailPattern = Patterns.EMAIL_ADDRESS;
		Account[] accounts = AccountManager.get(this).getAccounts();
		for (Account account : accounts) {
			if (emailPattern.matcher(account.name).matches()) {
				String possibleEmail = account.name;
				if (possibleEmail.contains("gmail.com")) {
					return possibleEmail;
				}
			}
		}
		return "";
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		if (actionBar.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}