package com.angel.telopaso.listeners;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.angel.telopaso.R;
import com.angel.telopaso.utils.Constantes;

/**
 * Listener para gestionar los cambios de posicion mientras se realizan nuevos
 * envios
 * */
public class PositionTeLoPasoLocation implements LocationListener {

	private Activity activity;

	public PositionTeLoPasoLocation() {

	}

	public PositionTeLoPasoLocation(Activity activity) {

		this.activity = activity;
	}

	@Override
	public void onLocationChanged(Location location) {

		TextView txtLocation = (TextView) activity.findViewById(R.id.txtLocalizacion);
		if (txtLocation != null) {
			StringBuffer sbLocation = new StringBuffer();
			sbLocation.append(location.getLatitude()).append(Constantes.DOS_PUNTOS).append(location.getLongitude());
			txtLocation.setText(sbLocation);
		}

	}

	@Override
	public void onProviderDisabled(String arg0) {
		Toast.makeText(activity, R.string.error_provider, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onProviderEnabled(String arg0) {

	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {

	}

}
