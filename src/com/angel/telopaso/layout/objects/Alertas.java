package com.angel.telopaso.layout.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * POJO que contiene los datos de las alertas recibidas al dispositivo
 * */
public class Alertas implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3458067607157474258L;

	private String idAlerta;
	private String nomAlerta;
	private String imagenEstado;
	private List<Contenido> listContenido = new ArrayList<Contenido>();

	public String getNomAlerta() {
		return nomAlerta;
	}

	public void setNomAlerta(String nomAlerta) {
		this.nomAlerta = nomAlerta;
	}

	public String getImagenEstado() {
		return imagenEstado;
	}

	public void setImagenEstado(String imagenEstado) {
		this.imagenEstado = imagenEstado;
	}

	public List<Contenido> getListContenido() {
		return listContenido;
	}

	public void setListContenido(List<Contenido> listContenido) {
		this.listContenido = listContenido;
	}

	public void addContenido(Contenido contenido) {
		this.listContenido.add(contenido);
	}

	public String getIdAlerta() {
		return idAlerta;
	}

	public void setIdAlerta(String idAlerta) {
		this.idAlerta = idAlerta;
	}

}
