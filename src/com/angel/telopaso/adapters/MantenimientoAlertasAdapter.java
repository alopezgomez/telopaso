package com.angel.telopaso.adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.angel.telopaso.R;
import com.angel.telopaso.layout.objects.Alerta;

/**
 * Adapter que presenta los datos de las alertas definidas por el usuario
 * */
public class MantenimientoAlertasAdapter extends BaseAdapter {

	private List<Alerta> listAlertas;
	private Context ctx;

	public MantenimientoAlertasAdapter() {
		super();
	}

	public MantenimientoAlertasAdapter(List<Alerta> listAlertas, Context ctx) {
		super();
		this.listAlertas = listAlertas;
		this.ctx = ctx;
	}

	@Override
	public int getCount() {
		return listAlertas.size();
	}

	@Override
	public Object getItem(int itemPosition) {
		return listAlertas.get(itemPosition);
	}

	@Override
	public long getItemId(int itemPosition) {
		return itemPosition;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.layout_alertas_principal, parent, false);
		}

		Alerta alerta = listAlertas.get(position);
		TextView txtTitulo = (TextView) v.findViewById(R.id.txtAlertas);
		txtTitulo.setText(alerta.getNombre());
		txtTitulo.setTag(alerta.getIdAlerta());
		v.setTag(alerta);
		return v;
	}

}
