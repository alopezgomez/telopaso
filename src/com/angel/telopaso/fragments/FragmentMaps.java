package com.angel.telopaso.fragments;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.api.IMapController;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay.OnItemGestureListener;
import org.osmdroid.views.overlay.ItemizedOverlayWithFocus;
import org.osmdroid.views.overlay.MinimapOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.PathOverlay;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.angel.telopaso.R;
import com.angel.telopaso.fragments.factory.FragmentFactory;
import com.angel.telopaso.layout.objects.Alerta;
import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.listeners.PositionTeLoPasoLocation;
import com.angel.telopaso.utils.Constantes;

/**
 * Fragmento que muestra el un mapa basado en Open Street Map para la busqueda y
 * la visualización de la distancia del usuario con el objeto a recoger
 * */
public class FragmentMaps extends Fragment {

	private MapView map;
	private Resources resources;
	private Alerta alerta;
	private FragmentFactory factory;
	private View viewMaps;
	private EditText searchText;
	private IMapController controller;
	private GeoPoint geoPoint;
	private ArrayList<OverlayItem> overlayItemArray;
	private int menuToInflate = R.menu.menu_mapas;
	private Contenido contenido;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		viewMaps = inflater.inflate(R.layout.actitvty_maps, container, false);
		resources = getActivity().getResources();
		factory = FragmentFactory.getInstance(getActivity().getFragmentManager(), getActivity());
		map = (MapView) viewMaps.findViewById(R.id.mapOpen);
		setHasOptionsMenu(true);
		searchText = (EditText) viewMaps.findViewById(R.id.searchText);
		controller = map.getController();

		map.setBuiltInZoomControls(true);
		map.setClickable(true);
		map.setLongClickable(true);
		map.setMultiTouchControls(true);
		controller.setZoom(20);
		String locationProvider = LocationManager.GPS_PROVIDER;
		LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			mostrarAvisoActivacionGPS();
			locationProvider = LocationManager.NETWORK_PROVIDER;
		}
		Location localizacion = locationManager.getLastKnownLocation(locationProvider);
		locationManager.requestLocationUpdates(locationProvider, 1000, 0, new PositionTeLoPasoLocation(getActivity()));
		if (localizacion != null) {
			geoPoint = new GeoPoint(localizacion.getLatitude(), localizacion.getLongitude());
			controller.setCenter(geoPoint);

		}

		overlayItemArray = new ArrayList<OverlayItem>();
		if (geoPoint != null) {
			overlayItemArray.add(new OverlayItem("Tu estás aquí", "Localizacion", geoPoint));
		}

		if (getArguments() != null && !getArguments().isEmpty()) {
			if (getArguments().getSerializable(Constantes.ARGS_DATOS) instanceof Alerta) {
				alerta = (Alerta) getArguments().getSerializable(Constantes.ARGS_DATOS);
				Toast.makeText(getActivity(), R.string.pulsar_seleccionar, Toast.LENGTH_LONG).show();

			} else {
				getActivity().setTitle(R.string.titulo_ver_loc);
				menuToInflate = R.menu.menu_mapas_lo_recojo;
				contenido = (Contenido) getArguments().getSerializable(Constantes.ARGS_DATOS);
				searchText.setVisibility(View.INVISIBLE);
				GeoPoint geoPointItem = new GeoPoint(Double.valueOf(contenido.getPosicionX()), Double.valueOf(contenido
						.getPosicionY()));
				overlayItemArray.add(new OverlayItem(contenido.getTitulo(), contenido.getDescCotenido(), geoPointItem));

				PathOverlay pathOverlay = new PathOverlay(Color.BLUE, getActivity());
				pathOverlay.addPoint(geoPoint);
				pathOverlay.addPoint(geoPointItem);
				map.getOverlays().add(pathOverlay);

			}
			getArguments().remove(Constantes.ARGS_DATOS);

		}

		ItemizedOverlayWithFocus<OverlayItem> itemOverlayIcon = new ItemizedOverlayWithFocus<OverlayItem>(
				getActivity(), overlayItemArray, new ItemGestureListener());
		map.getOverlays().add(itemOverlayIcon);

		MinimapOverlay miniMapOverlay = new MinimapOverlay(getActivity(), map.getTileRequestCompleteHandler());
		miniMapOverlay.setZoomDifference(5);
		miniMapOverlay.setHeight(200);
		miniMapOverlay.setWidth(200);
		map.getOverlays().add(miniMapOverlay);
		return viewMaps;

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		getActivity().getMenuInflater().inflate(menuToInflate, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menuCancelSearchLocation:
			factory.selectFragment(Constantes.FRAGMENT_CREACION_ALETTA, alerta);
			break;
		case R.id.menuCancelViewLocation:
			factory.selectFragment(Constantes.FRAGMENT_LORECOJO, contenido);
			break;
		case R.id.menuSearchLocation:
			String searchFor = searchText.getText().toString();
			JSONArray results = searchLocation(searchFor);

			if (results.length() > 0) {
				try {
					JSONObject firstResult = (JSONObject) results.get(0);
					Double lat = firstResult.getDouble("lat");
					Double lon = firstResult.getDouble("lon");

					GeoPoint point = new GeoPoint((int) (lat * 1E6), (int) (lon * 1E6));

					controller.setZoom(15);
					controller.setCenter(point);
					alerta.setLatitud(String.valueOf(lat));
					alerta.setLongitud(String.valueOf(lon));
					overlayItemArray.add(new OverlayItem("Localización buscada", "Localización buscada", geoPoint));
					map.invalidate();

				} catch (JSONException e) {
					Log.e("OnClickListener", e.getMessage());
				}
			} else {
				Toast.makeText(getActivity(), "No se econtraron resultados", Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}

		return true;
	}

	/**
	 * Metodo que muestra una alerta para la activacion del gps si se desea un
	 * mejor posicionamiento
	 * */
	private void mostrarAvisoActivacionGPS() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder
				.setMessage(resources.getString(R.string.activar_gps))
				.setCancelable(false)
				.setPositiveButton(resources.getString(R.string.btn_activar_gps),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent callGPSSettingIntent = new Intent(
										android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(callGPSSettingIntent);
							}
						});
		alertDialogBuilder.setNegativeButton(resources.getString(R.string.btn_cancelar_gps),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}

	/**
	 * Metodo que realiza una llamada http a la url expuesta por open street
	 * maps para la obtencion de direcciones
	 * 
	 * @param query
	 * */
	private JSONArray searchLocation(String query) {
		JSONArray results = new JSONArray();

		try {
			query = URLEncoder.encode(query, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			return results;
		}
		String url = "http://nominatim.openstreetmap.org/search?";
		url += "q=" + query;
		url += "&format=json";

		HttpGet httpGet = new HttpGet(url);
		DefaultHttpClient httpClient = new DefaultHttpClient();

		try {
			HttpResponse response = httpClient.execute(httpGet);
			String content = EntityUtils.toString(response.getEntity(), "utf-8");
			results = new JSONArray(content);

		} catch (Exception e) {
			Log.e("searchLocation", "Error executing url: " + url + " ; " + e.getMessage(), e);
		}

		return results;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Clase interna que permite mostrar mensajes sobre las indicaciones
	 * mostradas en el mapa
	 * */
	class ItemGestureListener implements OnItemGestureListener<OverlayItem> {

		@Override
		public boolean onItemLongPress(int arg0, OverlayItem item) {
			Toast.makeText(
					getActivity(),
					item.getTitle() + "\n" + item.getSnippet() + "\n" + item.getPoint().getLatitudeE6() + " : "
							+ item.getPoint().getLongitudeE6(), Toast.LENGTH_LONG).show();

			return true;
		}

		@Override
		public boolean onItemSingleTapUp(int arg0, OverlayItem item) {
			Toast.makeText(
					getActivity(),
					item.getTitle() + "\n" + item.getSnippet() + "\n" + item.getPoint().getLatitudeE6() + " : "
							+ item.getPoint().getLongitudeE6(), Toast.LENGTH_LONG).show();

			return true;
		}

	}

}
