package com.angel.telopaso.layout.objects;

import java.io.Serializable;

/**
 * POJO que contiene los datos de las alertas configuradas por el usuario
 * */
public class Alerta implements Serializable {

	private static final long serialVersionUID = -2548663233198347607L;

	private int idAlerta;
	private String nombre;
	private Long radio;
	private String longitud;
	private String latitud;
	private String tag;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getRadio() {
		return radio;
	}

	public void setRadio(Long radio) {
		this.radio = radio;
	}

	public String getLongitud() {
		return longitud;
	}

	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}

	public String getLatitud() {
		return latitud;
	}

	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public int getIdAlerta() {
		return idAlerta;
	}

	public void setIdAlerta(int idAlerta) {
		this.idAlerta = idAlerta;
	}

}
