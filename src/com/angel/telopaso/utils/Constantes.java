package com.angel.telopaso.utils;

public final class Constantes {

	public static final String ID = "id";
	public static final String TITULO = "titulo";
	public static final String ARGS_DATOS = "datos";
	public static final String DOS_PUNTOS = ":";
	public static final String ID_MESSAGE = "idMessage";
	public static final String FE_MESSAGE = "feMessage";
	public static final String DESCRIPCION = "descripcion";
	public static final String CATEGORIA = "categoria";
	public static final String USUARIO = "usuario";
	public static final String POSICION = "posicion";
	public static final String FILE_UPLOAD = "fileUpload";
	public static final String FILE_EXTENSION = "fileExtension";
	public static final String ACCEPT_HEADER = "Accept";
	public static final String APPLICATION_JSON = "application/json";
	public static final String CONTENT_TYPE = "Content-type";
	public static final String CONTENIDO = "contenido";
	public static final String ENVIANDO = "Enviando...";
	public static final String UTF_8 = "UTF-8";

	public static final int FRAGMENT_LIST_NOTIFICACIONES = 0;
	public static final int FRAGMENT_TELOPASO = 1;
	public static final int FRAGMENT_MIS_TELOPASO = 2;
	public static final int FRAGMENT_MIS_RECOGIDAS = 3;
	public static final int FRAGMENT_MANTENIMIENTO_ALERTA = 4;
	public static final int FRAGMENT_CREACION_ALETTA = 5;
	public static final int FRAGMENT_MAPS = 6;
	public static final int FRAGMENT_LORECOJO = 7;
	public static final int RECIENTE = 5;
	public static final int POSIBLE = 10;

	public static final String ENTORNO_DESA = "10.0.2.2:8080";
	public static final String ENTORNO_PRO = "s17004564.onlinehome-server.info:8080";

	private Constantes() {
	}

}
