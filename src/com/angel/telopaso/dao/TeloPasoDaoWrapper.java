package com.angel.telopaso.dao;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.angel.telopaso.layout.objects.Alerta;
import com.angel.telopaso.layout.objects.User;
import com.angel.telopaso.utils.Constantes;

/**
 * Clase que envuelve los accesos a datos de la aplicacion
 * 
 * @author Angel
 * */
public class TeloPasoDaoWrapper extends InitDataBase {

	private static final String TABLE_ALERTAS = "ALERTAS";
	private static final String SELECT_USER = "SELECT _email from USER";
	private static final String SELECT_ALERTA = "SELECT _idAlerta , _nomAlerta , _posicion , _radio , _tagAlerta  FROM ALERTAS";
	private static final String SELECT_ALERTA_BY_ID = "SELECT _idAlerta , _nomAlerta , _posicion , _radio , _tagAlerta  FROM ALERTAS WHERE _idAlerta=?";
	private static final String WHERE_ID_ALERTA = "_idAlerta=?";

	public TeloPasoDaoWrapper(Context context) {
		super(context);
	}

	/**
	 * Metodo que crea el usuario en base de datos
	 * 
	 * @param usuario
	 * */
	public void createUser(User usuario) {
		SQLiteDatabase dbWrite = super.getWritableDatabase();
		ContentValues nuevoRegistro = new ContentValues();
		nuevoRegistro.put("_email", usuario.getEmailUser());
		dbWrite.insert("USER", null, nuevoRegistro);
		dbWrite.close();
	}

	/**
	 * Metodo que crea una alerta en base de datos
	 * 
	 * @param alerta
	 * */
	public void createAlerta(Alerta alerta) {
		SQLiteDatabase dbWrite = super.getWritableDatabase();
		ContentValues nuevoRegistro = new ContentValues();
		nuevoRegistro.put("_nomAlerta", alerta.getNombre());
		StringBuffer sbPosicion = new StringBuffer();
		sbPosicion.append(alerta.getLatitud()).append(Constantes.DOS_PUNTOS).append(alerta.getLongitud());
		nuevoRegistro.put("_posicion", sbPosicion.toString());
		nuevoRegistro.put("_radio", alerta.getRadio());
		nuevoRegistro.put("_tagAlerta", alerta.getTag());
		dbWrite.insert(TABLE_ALERTAS, null, nuevoRegistro);
		dbWrite.close();
	}

	/**
	 * Metodo que indica si existe un usuario en la base de datos
	 * 
	 * @return true si existe falso si existe
	 * */
	public boolean existsUser() {

		return getUserName() != "" ? true : false;
	}

	/**
	 * Metodo que retorna el email del usuario
	 * 
	 * @return el usuario
	 * */
	public String getUserName() {

		SQLiteDatabase bd = super.getReadableDatabase();
		Cursor cursorListas = bd.rawQuery(SELECT_USER, new String[] {});
		String datos = "";
		if (cursorListas.moveToFirst()) {
			datos = cursorListas.getString(0);
		}
		cursorListas.close();
		bd.close();
		return datos;
	}

	/**
	 * Metodo que retorna las alertas configuradas por el usuario
	 * 
	 * @return Lista de alertas
	 * */
	public List<Alerta> getAlertas() {
		SQLiteDatabase bd = super.getReadableDatabase();
		Cursor cursosAlertas = bd.rawQuery(SELECT_ALERTA, new String[] {});
		List<Alerta> listAlertas = new ArrayList<Alerta>();

		while (cursosAlertas.moveToNext()) {
			Alerta alerta = populateAlerta(cursosAlertas);
			listAlertas.add(alerta);
		}
		cursosAlertas.close();
		bd.close();

		return listAlertas;

	}

	/**
	 * Metodo que realiza el borrado de una alerta a partir de su id
	 * 
	 * @param idAlerta
	 * */
	public void deleteAlerta(int idAlerta) {
		SQLiteDatabase dbWrite = super.getWritableDatabase();
		dbWrite.delete(TABLE_ALERTAS, WHERE_ID_ALERTA, new String[] { String.valueOf(idAlerta) });
		dbWrite.close();

	}

	/**
	 * Metodo que obtiene los datos de una alerta concreta a partir de su id
	 * 
	 * @param idAlerta
	 * @return {@link Alerta}
	 * */
	public Alerta getAlertById(String idAlerta) {
		SQLiteDatabase bd = super.getReadableDatabase();
		Cursor cursor = bd.rawQuery(SELECT_ALERTA_BY_ID, new String[] { idAlerta });
		Alerta alerta = new Alerta();
		if (cursor.moveToFirst()) {
			alerta = populateAlerta(cursor);
		}

		return alerta;
	}

	/**
	 * Metodo que realiza la actualización de una alerta en base de datos
	 * 
	 * @param alerta
	 *            {@link Alerta}
	 * */
	public void updateAlerta(Alerta alerta) {
		SQLiteDatabase bd = super.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("_nomAlerta", alerta.getNombre());
		StringBuffer sbPosicion = new StringBuffer();
		sbPosicion.append(alerta.getLatitud()).append(Constantes.DOS_PUNTOS).append(alerta.getLongitud());
		values.put("_posicion", sbPosicion.toString());
		values.put("_radio", alerta.getRadio());
		values.put("_tagAlerta", alerta.getTag());
		bd.update(TABLE_ALERTAS, values, WHERE_ID_ALERTA, new String[] { String.valueOf(alerta.getIdAlerta()) });
		bd.close();
	}

	private Alerta populateAlerta(Cursor cursosAlertas) {
		Alerta alerta = new Alerta();
		alerta.setIdAlerta(cursosAlertas.getInt(0));
		alerta.setNombre(cursosAlertas.getString(1));
		String posicion = cursosAlertas.getString(2);
		Log.d("id de alerta ", "la alerta tiene id " + alerta.getIdAlerta() + "  la posicion " + posicion);
		alerta.setLongitud(posicion.split(":")[0]);
		alerta.setLatitud(posicion.split(":")[1]);
		alerta.setRadio(Long.parseLong(cursosAlertas.getString(3)));
		alerta.setTag(cursosAlertas.getString(4));
		return alerta;
	}
}
