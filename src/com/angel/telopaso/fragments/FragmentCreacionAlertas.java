package com.angel.telopaso.fragments;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Arrays;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.angel.telopaso.R;
import com.angel.telopaso.dao.TeloPasoDaoWrapper;
import com.angel.telopaso.fragments.factory.FragmentFactory;
import com.angel.telopaso.layout.objects.Alerta;
import com.angel.telopaso.utils.Constantes;

/**
 * Fragmento que se encarga del control de la creacion y modificacion de las
 * alertas definidads por el usuario
 * */
public class FragmentCreacionAlertas extends Fragment {

	private FragmentFactory factory;
	private TeloPasoDaoWrapper teloPasoDaoWrapper;
	private View viewCreacionAlerta;
	private EditText textRadio;
	private EditText textIdAlerta;
	private TextView viewLat;
	private TextView viewLon;
	private EditText textNom;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		viewCreacionAlerta = inflater.inflate(R.layout.activity_creacion_alertas, container, false);
		factory = FragmentFactory.getInstance(getActivity().getFragmentManager(), getActivity());
		Button btnAbrirMaps = (Button) viewCreacionAlerta.findViewById(R.id.btn_google_maps);
		setHasOptionsMenu(true);
		teloPasoDaoWrapper = new TeloPasoDaoWrapper(getActivity());
		String[] categorias = getActivity().getResources().getStringArray(R.array.categorias);
		Spinner spnCategorias = (Spinner) viewCreacionAlerta.findViewById(R.id.spinnerCatAlerta);
		ArrayAdapter<String> adapterCategorias = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item, categorias);
		adapterCategorias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnCategorias.setAdapter(adapterCategorias);

		btnAbrirMaps.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Alerta alerta = new Alerta();
				populateAlertaFromView(alerta);
				factory.selectFragment(Constantes.FRAGMENT_MAPS, alerta);
			}
		});

		if (getArguments() != null) {
			Serializable args = getArguments().getSerializable(Constantes.ARGS_DATOS);
			Alerta alerta;
			if (args instanceof Integer) {
				alerta = teloPasoDaoWrapper.getAlertById(String.valueOf(args));
			} else {
				alerta = (Alerta) args;
			}

			textRadio = (EditText) viewCreacionAlerta.findViewById(R.id.txtRadioKm);
			textIdAlerta = (EditText) viewCreacionAlerta.findViewById(R.id.txtIdAlerta);
			viewLat = (TextView) viewCreacionAlerta.findViewById(R.id.txtLatitudSelec);
			viewLon = (TextView) viewCreacionAlerta.findViewById(R.id.txtLongitudSeleccionada);
			textNom = (EditText) viewCreacionAlerta.findViewById(R.id.txtNomAlerta);

			textIdAlerta.setText(alerta.getIdAlerta() > 0 ? String.valueOf(alerta.getIdAlerta()) : "");
			textRadio.setText(alerta.getRadio() != null ? String.valueOf(alerta.getRadio()) : "");
			viewLat.setText(alerta.getLatitud());
			textNom.setText(alerta.getNombre());
			viewLon.setText(alerta.getLongitud());
			spnCategorias.setSelection(Arrays.asList(categorias).indexOf(alerta.getTag()));

		}

		return viewCreacionAlerta;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		getActivity().getMenuInflater().inflate(R.menu.menu_creacion_alertas, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		int idItem = item.getItemId();
		EditText textIdAlerta = (EditText) viewCreacionAlerta.findViewById(R.id.txtIdAlerta);

		switch (idItem) {
		case R.id.menuSaveAlerta:
			Alerta alerta = new Alerta();
			populateAlertaFromView(alerta);

			if (textIdAlerta.getText().toString().trim().length() > 0) {
				teloPasoDaoWrapper.updateAlerta(alerta);
			} else {
				teloPasoDaoWrapper.createAlerta(alerta);
			}

			clearData(textIdAlerta);
			factory.selectFragment(Constantes.FRAGMENT_MANTENIMIENTO_ALERTA);
			break;
		case R.id.menuCancelAlerta:
			clearData(textIdAlerta);
			factory.selectFragment(Constantes.FRAGMENT_MANTENIMIENTO_ALERTA);
			break;
		default:
			break;
		}

		return true;

	}

	private void clearData(EditText textIdAlerta) {
		textIdAlerta.setText("");
		textRadio.setText("");
		viewLat.setText("");
		textNom.setText("");
		viewLon.setText("");
	}

	/**
	 * Metodo que carga los datos por referencia en un objeto de tipo
	 * {@link Alerta} para su tratamiento en base de datos
	 * 
	 * @param alerta
	 * */
	private void populateAlertaFromView(Alerta alerta) {
		EditText textRadio = (EditText) viewCreacionAlerta.findViewById(R.id.txtRadioKm);
		TextView viewLat = (TextView) viewCreacionAlerta.findViewById(R.id.txtLatitudSelec);
		TextView viewLon = (TextView) viewCreacionAlerta.findViewById(R.id.txtLongitudSeleccionada);
		EditText textNom = (EditText) viewCreacionAlerta.findViewById(R.id.txtNomAlerta);
		Spinner spnCategorias = (Spinner) viewCreacionAlerta.findViewById(R.id.spinnerCatAlerta);
		EditText textIdAlerta = (EditText) viewCreacionAlerta.findViewById(R.id.txtIdAlerta);

		alerta.setIdAlerta(textIdAlerta.getText().toString().trim().length() > 0 ? Integer.valueOf(textIdAlerta
				.getText().toString()) : 0);
		alerta.setLatitud(viewLat.getText().toString());
		alerta.setLongitud(viewLon.getText().toString());
		alerta.setNombre(textNom.getText().toString());
		Long radio;
		if (textRadio.getText().toString().trim().length() > 0)
			radio = Long.valueOf(textRadio.getText().toString());
		else
			radio = 0L;
		alerta.setRadio(radio);
		alerta.setTag(spnCategorias.getSelectedItem().toString());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
