package com.angel.telopaso.listeners;

import android.app.Activity;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.angel.telopaso.R;
import com.angel.telopaso.fragments.factory.FragmentFactory;

/**
 * Listener encargado de mostrar un fragmento seleccionado en el menu de la
 * aplicacion
 * */
public class DrawItemClickListener implements ListView.OnItemClickListener {

	private FragmentFactory fragmentFactory;
	private Activity activity;

	public DrawItemClickListener(FragmentFactory fragmentFactory, Activity activity) {
		this.fragmentFactory = fragmentFactory;
		this.activity = activity;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		fragmentFactory.selectFragment(position);
		ListView listOpciones = (ListView) activity.findViewById(R.id.opciones);
		listOpciones.setItemChecked(position, true);
		DrawerLayout drawerLayout = (DrawerLayout) activity.findViewById(R.id.drawLayout);
		drawerLayout.closeDrawer(listOpciones);
	}

}
