package com.angel.telopaso.fragments;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.angel.telopaso.R;
import com.angel.telopaso.adapters.UsersTeloPasoAdapter;
import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.utils.Constantes;

/**
 * Fragmento que controla la carga de la lista de envios realizados por el
 * usuario
 * **/
public class FragmentMisEnvios extends Fragment {

	private UsersTeloPasoAdapter usersAdapter;
	private ListView listView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View viewPrin = inflater.inflate(R.layout.activity_mias, container, false);

		SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
		String userName = preferences.getString("userName", "");

		new ListUserSendAsync().execute(userName);

		listView = (ListView) viewPrin.findViewById(R.id.listMios);

		return viewPrin;
	}

	/**
	 * Tarea asincrona que se encarga de carga la lista de de notificaciones
	 * creadas por el usuario
	 * 
	 * @author angel
	 * */
	class ListUserSendAsync extends AsyncTask<String, Void, List<Contenido>> {

		private Context ctx = FragmentMisEnvios.this.getActivity();

		private final ProgressDialog dialog = new ProgressDialog(FragmentMisEnvios.this.getActivity());

		@Override
		protected void onPostExecute(List<Contenido> result) {

			usersAdapter = new UsersTeloPasoAdapter(getActivity(), result);
			listView.setAdapter(usersAdapter);

			dialog.dismiss();

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Cargano...");
			dialog.show();
		}

		@Override
		protected List<Contenido> doInBackground(String... params) {

			String userName = params[0];
			List<Contenido> listDatos = new ArrayList<Contenido>();
			try {
				DefaultHttpClient httpClient = new DefaultHttpClient();
				String entorno = Constantes.ENTORNO_DESA;
				if (!Build.PRODUCT.contains("sdk")) {
					entorno = Constantes.ENTORNO_PRO;
				}
				HttpGet get = new HttpGet("http://" + entorno + "/server/api/send/messages/users/" + userName);
				HttpResponse response = httpClient.execute(get);
				if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					Toast.makeText(ctx, "no se ha podido conectar con el servidor", Toast.LENGTH_SHORT).show();
				}

				JSONArray jsonArray = new JSONArray(EntityUtils.toString(response.getEntity()));

				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject jsonObject = jsonArray.getJSONObject(i);
					Contenido contenido = new Contenido();
					contenido.setCategoria(jsonObject.getString(Constantes.CATEGORIA));
					contenido.setDescCotenido(jsonObject.getString(Constantes.DESCRIPCION));
					contenido.setFeContenido(jsonObject.getString(Constantes.FE_MESSAGE));
					contenido.setIdContenido(jsonObject.getString(Constantes.ID_MESSAGE));
					JSONArray posicionArray = jsonObject.getJSONArray(Constantes.POSICION);
					contenido.setCoordenadas(posicionArray.getString(0) + Constantes.DOS_PUNTOS
							+ posicionArray.getString(1));
					contenido.setTitulo(jsonObject.getString(Constantes.TITULO));
					listDatos.add(contenido);
				}

			} catch (IOException e) {
				Log.e(this.getClass().getName(), "Error al intentar acceder al servidor", e);

			} catch (JSONException e) {
				Log.e(this.getClass().getName(), "Error al transformar los datos servidor", e);
			}
			return listDatos;

		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}
