package com.angel.telopaso.senders;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.utils.Constantes;

/**
 * Tarea asincrona que sen encarga de realizar la llamada al servidor para la
 * recogida de una notificacion
 * */
public class LoCojoPutAsyncSender extends AsyncTask<Contenido, Void, String> {

	private final ProgressDialog dialog;

	public LoCojoPutAsyncSender(Context ctx) {
		dialog = new ProgressDialog(ctx);
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		dialog.dismiss();

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setMessage(Constantes.ENVIANDO);
		dialog.show();
	}

	@Override
	protected String doInBackground(Contenido... params) {
		Contenido contentToSend = params[0];

		try {

			DefaultHttpClient client = new DefaultHttpClient();
			String entorno = Constantes.ENTORNO_DESA;
			if (!Build.PRODUCT.contains("sdk")) {
				entorno = Constantes.ENTORNO_PRO;
			}
			HttpPut put = new HttpPut("http://" + entorno + "/server/api/receive/pick/"
					+ contentToSend.getIdContenido());

			HttpEntity entity = new StringEntity(contentToSend.getUsuario(), Constantes.UTF_8);
			put.setEntity(entity);

			put.setHeader(Constantes.ACCEPT_HEADER, Constantes.APPLICATION_JSON);
			put.setHeader(Constantes.CONTENT_TYPE, Constantes.APPLICATION_JSON);
			HttpResponse response = client.execute(put);
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				String mesageServer = EntityUtils.toString(response.getEntity());
				Log.e(this.getClass().getName(), "Error al enviar un mensaje al servidor con respuesta " + mesageServer);
				return String.valueOf(response.getStatusLine().getStatusCode());
			}
			return String.valueOf(HttpStatus.SC_OK);
		} catch (IOException e) {
			Log.e(this.getClass().getName(), "Error al intentar acceder al servidor", e);
		}

		return String.valueOf(HttpStatus.SC_INTERNAL_SERVER_ERROR);
	}

}
