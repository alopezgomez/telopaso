package com.angel.telopaso.senders;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.utils.Constantes;

/**
 * Tarea asincrona que sen encarga de realizar la llamada al servidor para el
 * rechazo de una notificacion
 * */
public class NoLoCojoPutAsyncSender extends AsyncTask<Contenido, Void, String> {

	private Context ctx;
	private final ProgressDialog dialog;

	public NoLoCojoPutAsyncSender(Context ctx) {
		this.ctx = ctx;
		dialog = new ProgressDialog(ctx);
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		dialog.dismiss();

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setMessage(Constantes.ENVIANDO);
		dialog.show();
	}

	@Override
	protected String doInBackground(Contenido... params) {
		Contenido contentToSend = params[0];

		try {

			DefaultHttpClient client = new DefaultHttpClient();
			String entorno = Constantes.ENTORNO_DESA;
			if (!Build.PRODUCT.contains("sdk")) {
				entorno = Constantes.ENTORNO_PRO;
			}
			HttpPut put = new HttpPut("http://" + entorno + "/server/api/receive/read/"
					+ contentToSend.getIdContenido());

			put.setEntity(new StringEntity(contentToSend.getUsuario(), Constantes.UTF_8));

			put.setHeader(Constantes.ACCEPT_HEADER, Constantes.APPLICATION_JSON);
			put.setHeader(Constantes.CONTENT_TYPE, Constantes.APPLICATION_JSON);
			HttpResponse response = client.execute(put);
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				Toast.makeText(ctx, "No se ha podido enviar el lo no lo cojo", Toast.LENGTH_SHORT).show();
				String mesageServer = EntityUtils.toString(response.getEntity());
				Log.e(this.getClass().getName(), "Error al enviar un mensaje al servidor con respuesta " + mesageServer);
				return String.valueOf(response.getStatusLine().getStatusCode());
			}
			return String.valueOf(HttpStatus.SC_OK);
		} catch (IOException e) {
			Toast.makeText(ctx, "Error al intentar acceder al servidor", Toast.LENGTH_SHORT).show();
			Log.e(this.getClass().getName(), "Error al intentar acceder al servidor", e);
		}

		return String.valueOf(HttpStatus.SC_INTERNAL_SERVER_ERROR);

	}

}
