package com.angel.telopaso.fragments;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.angel.telopaso.R;
import com.angel.telopaso.fragments.factory.FragmentFactory;
import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.listeners.TeloPasoClickListener;
import com.angel.telopaso.listeners.TeloPasoClickListener.TeloPasoClickListenerParameter;
import com.angel.telopaso.senders.NoLoCojoPutAsyncSender;
import com.angel.telopaso.utils.Constantes;

/**
 * Fragmento que se encarga de gestionar la recogida o el rechazo de una
 * notificacion
 * */
public class FragmentLoRecojo extends Fragment {

	private View viewNuevo;
	private ImageView img;
	private Button btnTeloPaso;
	private Spinner spnCategorias;
	private EditText txtTitulo;
	private EditText txtDesc;
	private TextView txtPosicion;
	private String userName;
	private String[] categorias;
	private Contenido contenido;
	private Button btnNoLoQuiero;
	private FragmentFactory fragmentFactory;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		viewNuevo = inflater.inflate(R.layout.activity_nuevo_envio, container, false);
		SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
		userName = preferences.getString("userName", "");
		fragmentFactory = FragmentFactory.getInstance(getActivity().getFragmentManager(), getActivity());

		setHasOptionsMenu(true);
		categorias = getActivity().getResources().getStringArray(R.array.categorias);
		spnCategorias = (Spinner) viewNuevo.findViewById(R.id.spnCategorias);
		img = (ImageView) viewNuevo.findViewById(R.id.imagenCamara);
		ArrayAdapter<String> adapterCategorias = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item, categorias);
		adapterCategorias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnCategorias.setAdapter(adapterCategorias);
		txtTitulo = (EditText) viewNuevo.findViewById(R.id.txtTitulo);
		txtDesc = (EditText) viewNuevo.findViewById(R.id.txtDescr);
		txtPosicion = (TextView) viewNuevo.findViewById(R.id.txtLocalizacion);
		btnTeloPaso = (Button) viewNuevo.findViewById(R.id.btnOk);
		btnNoLoQuiero = (Button) viewNuevo.findViewById(R.id.btnCancel);

		contenido = (Contenido) getArguments().getSerializable(Constantes.ARGS_DATOS);
		contenido.setUsuario(userName);
		List<String> listContenido = Arrays.asList(categorias);
		int selectedCategoria = listContenido.indexOf(contenido.getCategoria());
		spnCategorias.setSelection(selectedCategoria);
		Bitmap image = BitmapFactory
				.decodeByteArray(contenido.getContenidoImg(), 0, contenido.getContenidoImg().length);
		img.setImageBitmap(image);

		txtDesc.setText(contenido.getDescCotenido());
		txtDesc.setEnabled(false);

		txtTitulo.setText(contenido.getTitulo());
		txtTitulo.setEnabled(false);

		txtPosicion.setText(contenido.getCoordenadas());

		viewNuevo.setTag(contenido);

		btnTeloPaso.setOnClickListener(new TeloPasoClickListener(new TeloPasoClickListenerParameter(viewNuevo,
				getActivity(), 1, userName)));
		btnNoLoQuiero.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				new NoLoCojoPutAsyncSender(getActivity()).execute(contenido);

			}
		});
		getArguments().remove(Constantes.ARGS_DATOS);

		return viewNuevo;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		getActivity().getMenuInflater().inflate(R.menu.menu_lo_recojo, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menuMostarLoc:
			fragmentFactory.selectFragment(Constantes.FRAGMENT_MAPS, contenido);

			break;
		case R.id.menuLoRecojo:
			btnTeloPaso.callOnClick();
			fragmentFactory.selectFragment(Constantes.FRAGMENT_LIST_NOTIFICACIONES);

			break;
		case R.id.menuNoLoRecojo:
			btnNoLoQuiero.callOnClick();
			fragmentFactory.selectFragment(Constantes.FRAGMENT_LIST_NOTIFICACIONES);

			break;

		default:
			break;
		}

		return true;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}
}
