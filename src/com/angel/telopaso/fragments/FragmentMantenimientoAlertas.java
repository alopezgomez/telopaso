package com.angel.telopaso.fragments;

import java.lang.reflect.Field;
import java.util.List;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.angel.telopaso.R;
import com.angel.telopaso.adapters.MantenimientoAlertasAdapter;
import com.angel.telopaso.dao.TeloPasoDaoWrapper;
import com.angel.telopaso.fragments.factory.FragmentFactory;
import com.angel.telopaso.layout.objects.Alerta;
import com.angel.telopaso.utils.Constantes;

/**
 * Fragmento que se encarga de la carga de la lista de alertas definidas por el
 * usuario para su mantenimiento
 ***/
public class FragmentMantenimientoAlertas extends Fragment {

	private MantenimientoAlertasAdapter adapter;
	private FragmentFactory factory;
	private TeloPasoDaoWrapper daoWrapper;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View mantenimiento = inflater.inflate(R.layout.activity_mantenimiento_alertas, container, false);
		daoWrapper = new TeloPasoDaoWrapper(getActivity());
		List<Alerta> listAlertas = daoWrapper.getAlertas();
		factory = FragmentFactory.getInstance(getActivity().getFragmentManager(), getActivity());
		setHasOptionsMenu(true);

		ListView listViewAlertas = (ListView) mantenimiento.findViewById(R.id.listAlertas);
		adapter = new MantenimientoAlertasAdapter(listAlertas, getActivity());
		listViewAlertas.setAdapter(adapter);
		registerForContextMenu(listViewAlertas);
		return mantenimiento;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		getActivity().getMenuInflater().inflate(R.menu.menu_lista_alertas, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Alerta alertas = new Alerta();
		factory.selectFragment(Constantes.FRAGMENT_CREACION_ALETTA, alertas);
		return true;

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		ListView view = (ListView) v;
		Alerta alerta = (Alerta) view.getItemAtPosition(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
		Intent intent = new Intent();
		intent.putExtra(Constantes.ID, alerta.getIdAlerta());
		intent.putExtra(Constantes.TITULO, alerta.getNombre());
		menu.setHeaderTitle(R.string.contextMenu);
		MenuItem eliminar = menu.add(Menu.NONE, 1, 0, R.string.menu_eliminar);
		eliminar.setIntent(intent);
		MenuItem modificar = menu.add(Menu.NONE, 2, 0, R.string.menu_modificar);
		modificar.setIntent(intent);
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int idAlerta = item.getIntent().getExtras().getInt(Constantes.ID);
		switch (item.getItemId()) {
		case 1:
			daoWrapper.deleteAlerta(idAlerta);
			break;
		case 2:
			factory.selectFragment(Constantes.FRAGMENT_CREACION_ALETTA, Integer.valueOf(idAlerta));
			break;

		default:
			break;
		}
		adapter.notifyDataSetChanged();
		return true;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}
