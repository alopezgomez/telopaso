package com.angel.telopaso.service;

import java.io.IOException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.angel.telopaso.R;
import com.angel.telopaso.dao.TeloPasoDaoWrapper;
import com.angel.telopaso.layout.objects.Alerta;
import com.angel.telopaso.utils.Constantes;

/**
 * Servicio encargado de mostrar las notificaciones al usuario en la zona de
 * notificacion del dispositivo, este servicio tiene un timer que realiza
 * peticiones periodicas al servidor para obtener informacion actualizada
 * 
 * */
public class ServiceSender extends Service {

	private List<Alerta> listAlertas;
	private String usuario;
	private NotificationManager notificationManager;
	private Intent intent;
	private TeloPasoDaoWrapper daoWrapper;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		daoWrapper = new TeloPasoDaoWrapper(getApplicationContext());
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		listAlertas = daoWrapper.getAlertas();
		if (intent != null && intent.getExtras() != null) {
			usuario = intent.getExtras().getString("userName");
		} else {
			SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			usuario = preferences.getString("userName", "");
		}
		timer();
		this.intent = intent;

		return super.onStartCommand(intent, flags, startId);
	}

	/**
	 * Metodo que crea un planificador en un hilo diferente para la comprobacion
	 * de notificaciones
	 * */
	private void timer() {

		final Handler handler = new Handler();
		Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						CheckNotificationsAsyncSender checkNotificationsAsyncSender = new CheckNotificationsAsyncSender();
						Object[] parms = new Object[] { listAlertas, usuario };
						checkNotificationsAsyncSender.execute(parms);
					}
				});

			}
		};
		timer.schedule(task, 1000, 100000);
	}

	/**
	 * Clase que realiza la comprobacion en el servidor si hay notificaciones,
	 * si existen las muestra al usuario
	 * */
	class CheckNotificationsAsyncSender extends AsyncTask<Object, Void, Integer> {

		@Override
		protected void onPostExecute(Integer result) {
			if (result > 0) {
				PendingIntent pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
				Notification noti = new Notification.Builder(getApplicationContext())
						.setContentTitle("Tienes " + result + " nuevas alertas").setContentText("Alerta")
						.setSmallIcon(R.drawable.posicion).setContentIntent(pIntent).build();
				notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				noti.flags |= Notification.FLAG_AUTO_CANCEL;
				notificationManager.notify(0, noti);
			}

			super.onPostExecute(result);
		}

		@Override
		protected Integer doInBackground(Object... params) {
			Integer numAlertas = Integer.valueOf(0);
			try {
				@SuppressWarnings("unchecked")
				List<Alerta> listDatos = (List<Alerta>) params[0];
				String entorno = Constantes.ENTORNO_DESA;
				if (!Build.PRODUCT.contains("sdk")) {
					entorno = Constantes.ENTORNO_PRO;
				}
				for (Alerta datos : listDatos) {
					String userName = (String) params[1];
					StringBuffer position = new StringBuffer();
					position.append(datos.getLongitud()).append(",").append(datos.getLatitud());
					DefaultHttpClient client = new DefaultHttpClient();
					HttpGet get = new HttpGet("http://" + entorno + "/server/api/send?posicion=" + position.toString()
							+ "&radio=" + datos.getRadio() + "&userName=" + userName);
					HttpResponse response = client.execute(get);
					String mesageServer = EntityUtils.toString(response.getEntity());

					if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
						Log.e(this.getClass().getName(), "Error al enviar un mensaje al servidor con respuesta "
								+ mesageServer);
					} else {
						numAlertas = numAlertas + Integer.valueOf(mesageServer);
					}
				}
			} catch (IOException e) {
				Log.e(this.getClass().getName(), "Error al intentar acceder al servidor", e);
			}
			return numAlertas;
		}

	}

}
