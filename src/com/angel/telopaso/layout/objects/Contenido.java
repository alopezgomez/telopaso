package com.angel.telopaso.layout.objects;

import java.io.Serializable;

import com.angel.telopaso.utils.Constantes;

/**
 * POJO que contiene el contenido de los datos para mostrar las notificaciones
 * al usuario
 * */
public class Contenido implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7048676565780608372L;

	private byte[] contenidoImg;
	private String extensionImg;
	private String posicionX;
	private String posicionY;
	private String descCotenido;
	private String idContenido;
	private String feContenido;
	private String titulo;
	private String categoria;
	private String usuario;

	public byte[] getContenidoImg() {
		return contenidoImg;
	}

	public void setContenidoImg(byte[] contenidoImg) {
		this.contenidoImg = contenidoImg;
	}

	public String getPosicionX() {
		return posicionX;
	}

	public void setPosicionX(String posicionX) {
		this.posicionX = posicionX;
	}

	public String getPosicionY() {
		return posicionY;
	}

	public void setPosicionY(String posicionY) {
		this.posicionY = posicionY;
	}

	public String getDescCotenido() {
		return descCotenido;
	}

	public void setDescCotenido(String descCotenido) {
		this.descCotenido = descCotenido;
	}

	public String getIdContenido() {
		return idContenido;
	}

	public void setIdContenido(String idContenido) {
		this.idContenido = idContenido;
	}

	public String getFeContenido() {
		return feContenido;
	}

	public void setFeContenido(String feContenido) {
		this.feContenido = feContenido;
	}

	public void setCoordenadas(String coordenadas) {
		this.posicionX = coordenadas.split(Constantes.DOS_PUNTOS)[0];
		this.posicionY = coordenadas.split(Constantes.DOS_PUNTOS)[1];
	}

	public String getCoordenadas() {
		StringBuffer coordenadas = new StringBuffer();
		coordenadas.append(getPosicionX()).append(Constantes.DOS_PUNTOS).append(getPosicionY());
		return coordenadas.toString();
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getExtensionImg() {
		return extensionImg;
	}

	public void setExtensionImg(String extensionImg) {
		this.extensionImg = extensionImg;
	}

}
