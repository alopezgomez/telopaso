package com.angel.telopaso.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Clase de utilidaes para la conversion de fecha a cadena y viceversa
 * */
public class FechaUtils {

	private final static SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy", Locale.US);

	private FechaUtils() {
	}

	public static String dateToString(Date fecha) {
		String fechaStr = sdf.format(fecha);
		return fechaStr;
	}

	public static Date stringToDate(String fechaStr) throws ParseException {
		Date fecha = sdf.parse(fechaStr);
		return fecha;
	}

}
