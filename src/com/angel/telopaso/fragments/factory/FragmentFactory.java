package com.angel.telopaso.fragments.factory;

import java.io.Serializable;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.util.SparseArray;

import com.angel.telopaso.R;
import com.angel.telopaso.fragments.FragmentCreacionAlertas;
import com.angel.telopaso.fragments.FragmentListaAlertasExpandible;
import com.angel.telopaso.fragments.FragmentLoRecojo;
import com.angel.telopaso.fragments.FragmentMantenimientoAlertas;
import com.angel.telopaso.fragments.FragmentMaps;
import com.angel.telopaso.fragments.FragmentMisEnvios;
import com.angel.telopaso.fragments.FragmentMisRecogidas;
import com.angel.telopaso.fragments.FragmentNuevoTeLoPaso;
import com.angel.telopaso.utils.Constantes;

/**
 * Clase factoria implementada a modo de singleton para la navegacion entre los
 * fragmentos de la aplicacion
 * */
public class FragmentFactory {

	private SparseArray<Fragment> mapaFragmentos = new SparseArray<Fragment>();
	private FragmentManager fragmentManager;
	private Activity activity;
	private String[] fragmento;
	private static FragmentFactory instance;
	private Fragment fragment;

	public static FragmentFactory getInstance(FragmentManager fragmentManager, Activity activiy) {
		if (instance == null) {
			instance = new FragmentFactory(fragmentManager, activiy);
		}
		return instance;
	}

	public static FragmentFactory getInstance() {
		return instance;
	}

	private FragmentFactory() {
		mapaFragmentos.append(Constantes.FRAGMENT_LIST_NOTIFICACIONES, new FragmentListaAlertasExpandible());
		mapaFragmentos.append(Constantes.FRAGMENT_TELOPASO, new FragmentNuevoTeLoPaso());
		mapaFragmentos.append(Constantes.FRAGMENT_MANTENIMIENTO_ALERTA, new FragmentMantenimientoAlertas());
		mapaFragmentos.append(Constantes.FRAGMENT_CREACION_ALETTA, new FragmentCreacionAlertas());
		mapaFragmentos.append(Constantes.FRAGMENT_MAPS, new FragmentMaps());
		mapaFragmentos.append(Constantes.FRAGMENT_MIS_RECOGIDAS, new FragmentMisRecogidas());
		mapaFragmentos.append(Constantes.FRAGMENT_MIS_TELOPASO, new FragmentMisEnvios());
		mapaFragmentos.append(Constantes.FRAGMENT_LORECOJO, new FragmentLoRecojo());

	}

	private FragmentFactory(FragmentManager fragmentManager, Activity activiy) {
		this();
		this.fragmentManager = fragmentManager;
		this.activity = activiy;
		this.fragmento = activiy.getResources().getStringArray(R.array.fragmentos);
	}

	public void selectFragment(int fragemento, Serializable... arg) {
		fragment = mapaFragmentos.get(fragemento);
		setFrameArguments(arg);
		fragmentManager.beginTransaction().replace(R.id.contenido, fragment).commit();
		activity.setTitle(fragmento[fragemento]);
	}

	private void setFrameArguments(Serializable... arg) {
		if (arg.length > 0) {
			Bundle args = new Bundle();
			args.putSerializable(Constantes.ARGS_DATOS, arg[0]);
			fragment.setArguments(args);
		}
	}

}
