package com.angel.telopaso.adapters;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.angel.telopaso.R;
import com.angel.telopaso.fragments.factory.FragmentFactory;
import com.angel.telopaso.layout.objects.Alertas;
import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.utils.Constantes;
import com.angel.telopaso.utils.FechaUtils;

/**
 * Adaptador para la carga de la vista de notificaciones Mostrando los datos de
 * las notificaciones y sus colores en funcion de la fecha de publicacion
 * */
public class AlertasAdapter extends BaseExpandableListAdapter {

	private Activity ctx;
	private List<Alertas> listAlertas;

	public AlertasAdapter(Activity ctx, List<Alertas> listAlertas) {
		this.ctx = ctx;
		this.listAlertas = listAlertas;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		Contenido contenido = getContenido(groupPosition, childPosition);
		return contenido;
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {

		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.layout_alertas_detalle, parent, false);
		}

		final Contenido contenido = getContenido(groupPosition, childPosition);

		TextView txtDescCont = (TextView) v.findViewById(R.id.txtDescContenido);
		TextView txtFeCont = (TextView) v.findViewById(R.id.txtFechaPubl);
		TextView txtPosiCont = (TextView) v.findViewById(R.id.txtPosicion);
		ImageView estado = (ImageView) v.findViewById(R.id.imgPrevContenido);
		Resources res = ctx.getResources();
		estado.setImageDrawable(res.getDrawable(android.R.drawable.ic_dialog_alert));
		try {

			int daysFePublica = (int) (FechaUtils.stringToDate(contenido.getFeContenido()).getTime() / (1000 * 60 * 60 * 24));
			String feParsedStr = FechaUtils.dateToString(Calendar.getInstance().getTime());
			int daysAct = (int) (FechaUtils.stringToDate(feParsedStr).getTime() / (1000 * 60 * 60 * 24));
			int days = daysAct - daysFePublica;

			if (days <= Constantes.RECIENTE) {
				estado.setBackgroundColor(Color.GREEN);
			} else if (days <= Constantes.POSIBLE) {
				estado.setBackgroundColor(Color.YELLOW);
			} else {
				estado.setBackgroundColor(Color.RED);
			}

		} catch (ParseException e) {
			Log.e(this.getClass().getName(), "Error al calcular el estado con las fechas", e);
		}

		txtDescCont.setText(contenido.getDescCotenido());
		txtFeCont.setText(contenido.getFeContenido());
		StringBuffer sbPosicion = new StringBuffer();
		sbPosicion.append(contenido.getPosicionX()).append("/").append(contenido.getPosicionY());
		txtPosiCont.setText(sbPosicion);
		v.setTag(contenido.getIdContenido());

		v.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				new MessageAsyncReceiver().execute(contenido.getIdContenido());
			}
		});
		return v;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		Alertas alerta = listAlertas.get(groupPosition);
		List<Contenido> listContenido = alerta.getListContenido();
		return listContenido.size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		Alertas alerta = listAlertas.get(groupPosition);
		return alerta;
	}

	@Override
	public int getGroupCount() {
		return listAlertas.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.layout_alertas_principal, parent, false);
		}

		Alertas alertas = listAlertas.get(groupPosition);
		TextView txtDescAlerta = (TextView) v.findViewById(R.id.txtAlertas);
		txtDescAlerta.setText(alertas.getNomAlerta());

		return v;
	}

	@Override
	public boolean hasStableIds() {
		return true;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {

		return true;
	}

	private Contenido getContenido(int groupPosition, int childPosition) {
		Alertas alerta = listAlertas.get(groupPosition);
		List<Contenido> listContenido = alerta.getListContenido();
		Contenido contenido = listContenido.get(childPosition);
		return contenido;
	}

	/**
	 * Clase interna que realiza una tarea asincrona para obtener los datos
	 * completos de una notificación cuando se selecciona una en la lista
	 * */
	class MessageAsyncReceiver extends AsyncTask<String, Void, Contenido> {

		private final ProgressDialog dialog = new ProgressDialog(ctx);

		@Override
		protected void onPostExecute(Contenido result) {
			dialog.dismiss();
			FragmentFactory factory = FragmentFactory.getInstance(ctx.getFragmentManager(), ctx);
			factory.selectFragment(Constantes.FRAGMENT_LORECOJO, result);
			super.onPostExecute(result);

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog.setMessage("Cargando...");
			dialog.show();
		}

		@Override
		protected Contenido doInBackground(String... params) {

			try {

				DefaultHttpClient client = new DefaultHttpClient();
				String entorno = Constantes.ENTORNO_DESA;
				if (!Build.PRODUCT.contains("sdk")) {
					entorno = Constantes.ENTORNO_PRO;
				}
				HttpGet get = new HttpGet("http://" + entorno + "/server/api/send/messages/" + params[0]);

				HttpResponse response = client.execute(get);
				if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
					String mesageServer = EntityUtils.toString(response.getEntity());
					Log.e(this.getClass().getName(), "Error al enviar un mensaje al servidor con respuesta "
							+ mesageServer);

					return null;
				}
				String mesageServer = EntityUtils.toString(response.getEntity());
				JSONObject jsonObject = new JSONObject(mesageServer);
				Contenido contenido = new Contenido();
				contenido.setCategoria(jsonObject.getString(Constantes.CATEGORIA));
				byte[] dataImg = Base64.decode(jsonObject.getString(Constantes.FILE_UPLOAD), Base64.DEFAULT);
				contenido.setContenidoImg(dataImg);
				contenido.setIdContenido(jsonObject.getString(Constantes.ID_MESSAGE));
				JSONArray posicion = jsonObject.getJSONArray(Constantes.POSICION);
				contenido.setCoordenadas(posicion.getString(0) + Constantes.DOS_PUNTOS + posicion.getString(1));
				contenido.setDescCotenido(jsonObject.getString(Constantes.DESCRIPCION));
				contenido.setTitulo(jsonObject.getString(Constantes.TITULO));

				return contenido;
			} catch (IOException e) {
				Log.e(this.getClass().getName(), "Error al intentar acceder al servidor", e);
			} catch (JSONException e) {
				Log.e(this.getClass().getName(), "Error al transformar los datos servidor", e);
			}

			return null;
		}
	}

}
