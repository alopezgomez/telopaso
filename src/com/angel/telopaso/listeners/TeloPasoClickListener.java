package com.angel.telopaso.listeners;

import java.io.ByteArrayOutputStream;
import java.util.Date;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.angel.telopaso.R;
import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.senders.LoCojoPutAsyncSender;
import com.angel.telopaso.senders.TeloPasoPostAsynSender;
import com.angel.telopaso.utils.FechaUtils;

/**
 * Listener para las acciones asociadas con las notificaciones, envio, recogida,
 * rechazo
 * */
public class TeloPasoClickListener implements OnClickListener {

	private View view;
	private Context ctx;
	private Contenido contenido;
	private int operacion;
	private String userName;

	public TeloPasoClickListener() {

	}

	public static class TeloPasoClickListenerParameter {
		public View view;
		public Context ctx;
		public int operacion;
		public String userName;

		public TeloPasoClickListenerParameter(View view, Context ctx, int operacion, String userName) {
			this.view = view;
			this.ctx = ctx;
			this.operacion = operacion;
			this.userName = userName;
		}
	}

	public TeloPasoClickListener(TeloPasoClickListenerParameter param) {
		this.view = param.view;
		this.ctx = param.ctx;
		this.operacion = param.operacion;
		this.userName = param.userName;
	}

	@Override
	public void onClick(View v) {
		String txtTitulo = ((EditText) view.findViewById(R.id.txtTitulo)).getText().toString();
		String txtDesc = ((EditText) view.findViewById(R.id.txtDescr)).getText().toString();
		String coordenadas = ((TextView) view.findViewById(R.id.txtLocalizacion)).getText().toString();
		ImageView img = (ImageView) view.findViewById(R.id.imagenCamara);
		Spinner categorias = (Spinner) view.findViewById(R.id.spnCategorias);

		if (operacion == 0) {
			if (!isValidContenido(txtTitulo, txtDesc, img, categorias)) {
				Toast.makeText(ctx, R.string.errorValidacion, Toast.LENGTH_LONG).show();
			} else {
				contenido = new Contenido();
				contenido.setCoordenadas(coordenadas);
				contenido.setDescCotenido(txtDesc);
				contenido.setFeContenido(FechaUtils.dateToString(new Date()));
				contenido.setTitulo(txtTitulo);
				contenido.setCategoria((String) categorias.getSelectedItem());
				Bitmap bmp = ((BitmapDrawable) img.getDrawable()).getBitmap();
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
				contenido.setContenidoImg(stream.toByteArray());
				contenido.setExtensionImg(".png");
				contenido.setUsuario(userName);
				new TeloPasoPostAsynSender(ctx).execute(contenido);
			}
		} else {
			contenido = (Contenido) view.getTag();
			new LoCojoPutAsyncSender(ctx).execute(contenido);
		}

		((EditText) view.findViewById(R.id.txtTitulo)).setText("");
		((EditText) view.findViewById(R.id.txtDescr)).setText("");
		((TextView) view.findViewById(R.id.txtLocalizacion)).setText("");
	}

	private boolean isValidContenido(String txtTitulo, String txtDesc, ImageView img, Spinner categorias) {
		return !TextUtils.isEmpty(txtTitulo) && !TextUtils.isEmpty(txtDesc) && hayImagen(img)
				&& isValidCategoriaSeleccionada(categorias);
	}

	private boolean isValidCategoriaSeleccionada(Spinner categorias) {
		return !categorias.getSelectedItem().equals(ctx.getResources().getString(R.string.categoria));
	}

	private boolean hayImagen(ImageView img) {
		return img.getDrawable() != null && ((BitmapDrawable) img.getDrawable()).getBitmap() != null;
	}

}
