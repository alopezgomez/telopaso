package com.angel.telopaso.senders;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.utils.Constantes;

/**
 * Tarea asincrona que sen encarga de realizar la llamada al servidor para el
 * envio de una notificacion
 */
public class TeloPasoPostAsynSender extends AsyncTask<Contenido, Void, String> {

	private Context ctx;
	private final ProgressDialog dialog;

	public TeloPasoPostAsynSender(Context ctx) {
		this.ctx = ctx;
		dialog = new ProgressDialog(ctx);
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog.setMessage(Constantes.ENVIANDO);
		dialog.show();
		dialog.dismiss();

	}

	@Override
	protected String doInBackground(Contenido... params) {

		Contenido contentToSend = params[0];

		try {

			DefaultHttpClient client = new DefaultHttpClient();
			String entorno = Constantes.ENTORNO_DESA;
			if (!Build.PRODUCT.contains("sdk")) {
				entorno = Constantes.ENTORNO_PRO;
			}
			HttpPost post = new HttpPost("http://" + entorno + "/server/api/receive");
			JSONObject jsonObject = setDataInJson(contentToSend);

			String stream = jsonObject.toString();
			post.setEntity(new StringEntity(stream, Constantes.UTF_8));

			post.setHeader(Constantes.ACCEPT_HEADER, Constantes.APPLICATION_JSON);
			post.setHeader(Constantes.CONTENT_TYPE, Constantes.APPLICATION_JSON);
			HttpResponse response = client.execute(post);
			if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
				Toast.makeText(ctx, "No se ha podido enviar el Te lo Paso", Toast.LENGTH_SHORT).show();
				String mesageServer = EntityUtils.toString(response.getEntity());
				Log.e(this.getClass().getName(), "Error al enviar un mensaje al servidor con respuesta " + mesageServer);
				return String.valueOf(response.getStatusLine().getStatusCode());
			}
			return String.valueOf(HttpStatus.SC_OK);
		} catch (IOException e) {
			Toast.makeText(ctx, "Error al intentar acceder al servidor", Toast.LENGTH_SHORT).show();
			Log.e(this.getClass().getName(), "Error al intentar acceder al servidor", e);
		} catch (JSONException e) {
			Toast.makeText(ctx, "Error al transformar los datos servidor", Toast.LENGTH_SHORT).show();
			Log.e(this.getClass().getName(), "Error al transformar los datos servidor", e);
		}

		return String.valueOf(HttpStatus.SC_INTERNAL_SERVER_ERROR);
	}

	/**
	 * Metodo que parsea el contenido de la notificacion a un formato JSON que
	 * es el consumido por el servidor
	 * 
	 * @param contentToSend
	 * */
	private JSONObject setDataInJson(Contenido contentToSend) throws JSONException {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put(Constantes.TITULO, contentToSend.getTitulo());
		jsonObject.put(Constantes.USUARIO, contentToSend.getUsuario());
		jsonObject.put(Constantes.DESCRIPCION, contentToSend.getDescCotenido());
		jsonObject.put(Constantes.CATEGORIA, contentToSend.getCategoria());
		jsonObject.put(Constantes.POSICION,
				new JSONArray().put(contentToSend.getPosicionX()).put(contentToSend.getPosicionY()));

		byte[] encodeBase64 = Base64.encode(contentToSend.getContenidoImg(), Base64.DEFAULT);
		jsonObject.put(Constantes.FILE_UPLOAD, new String(encodeBase64));
		jsonObject.put(Constantes.FILE_EXTENSION, contentToSend.getExtensionImg());
		return jsonObject;
	}
}
