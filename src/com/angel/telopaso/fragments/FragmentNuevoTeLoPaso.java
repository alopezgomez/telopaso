package com.angel.telopaso.fragments;

import java.lang.reflect.Field;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.angel.telopaso.R;
import com.angel.telopaso.fragments.factory.FragmentFactory;
import com.angel.telopaso.listeners.PositionTeLoPasoLocation;
import com.angel.telopaso.listeners.TeloPasoClickListener;
import com.angel.telopaso.listeners.TeloPasoClickListener.TeloPasoClickListenerParameter;
import com.angel.telopaso.utils.Constantes;

/**
 * Fragmento que se encarga de la creacion de nuevas alertas, permitiendo acceso
 * a la camara y a la posicion gps para el envio
 * */
public class FragmentNuevoTeLoPaso extends Fragment {

	private static final int CAMERA_PIC_REQUEST = 9999;
	private View viewNuevo;
	private ImageView img;
	private Button btnTeloPaso;
	private Spinner spnCategorias;
	private TextView txtPosicion;
	private Resources resources;
	private String userName;
	private String[] categorias;
	private FragmentFactory fragmentFactory;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		viewNuevo = inflater.inflate(R.layout.activity_nuevo_envio, container, false);
		fragmentFactory = FragmentFactory.getInstance(getActivity().getFragmentManager(), getActivity());

		resources = getActivity().getResources();
		SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
		userName = preferences.getString("userName", "");
		setHasOptionsMenu(true);
		categorias = getActivity().getResources().getStringArray(R.array.categorias);
		spnCategorias = (Spinner) viewNuevo.findViewById(R.id.spnCategorias);
		img = (ImageView) viewNuevo.findViewById(R.id.imagenCamara);
		ArrayAdapter<String> adapterCategorias = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item, categorias);
		adapterCategorias.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnCategorias.setAdapter(adapterCategorias);
		txtPosicion = (TextView) viewNuevo.findViewById(R.id.txtLocalizacion);
		btnTeloPaso = (Button) viewNuevo.findViewById(R.id.btnOk);

		btnTeloPaso.setOnClickListener(new TeloPasoClickListener(new TeloPasoClickListenerParameter(viewNuevo,
				getActivity(), 0, userName)));

		String locationProvider = LocationManager.GPS_PROVIDER;
		LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
		if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			mostrarAvisoActivacionGPS();
			locationProvider = LocationManager.NETWORK_PROVIDER;
		}
		Location localizacion = locationManager.getLastKnownLocation(locationProvider);
		locationManager.requestLocationUpdates(locationProvider, 1000, 0, new PositionTeLoPasoLocation(getActivity()));
		StringBuffer sbLocation = new StringBuffer();
		if (localizacion != null) {
			sbLocation.append(localizacion.getLatitude()).append(Constantes.DOS_PUNTOS)
					.append(localizacion.getLongitude());
			txtPosicion.setText(sbLocation);
		}

		img.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(intent, CAMERA_PIC_REQUEST);
			}
		});
		return viewNuevo;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		getActivity().getMenuInflater().inflate(R.menu.menu_te_lo_paso, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menuLoPaso:
			btnTeloPaso.callOnClick();
			fragmentFactory.selectFragment(Constantes.FRAGMENT_LIST_NOTIFICACIONES);

			break;
		case R.id.menuNoLoPaso:
			fragmentFactory.selectFragment(Constantes.FRAGMENT_LIST_NOTIFICACIONES);

			break;

		default:
			break;
		}

		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == CAMERA_PIC_REQUEST) {
			if (data.getExtras() != null) {
				Bitmap imagen = (Bitmap) data.getExtras().get("data");
				ImageView img = (ImageView) getActivity().findViewById(R.id.imagenCamara);
				img.setImageBitmap(imagen);
			}
		}
	}

	/**
	 * Metodo que muestra un aviso para indicar que si se activa el GPS se
	 * tendrá mejor posicionamiento
	 * */
	private void mostrarAvisoActivacionGPS() {
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder
				.setMessage(resources.getString(R.string.activar_gps))
				.setCancelable(false)
				.setPositiveButton(resources.getString(R.string.btn_activar_gps),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								Intent callGPSSettingIntent = new Intent(
										android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
								startActivity(callGPSSettingIntent);
							}
						});
		alertDialogBuilder.setNegativeButton(resources.getString(R.string.btn_cancelar_gps),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

}