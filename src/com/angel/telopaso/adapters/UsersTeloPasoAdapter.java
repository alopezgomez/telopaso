package com.angel.telopaso.adapters;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.angel.telopaso.R;
import com.angel.telopaso.layout.objects.Contenido;

/**
 * Adapter para la vista de la lista de publicaciones del usuario
 * */
public class UsersTeloPasoAdapter extends BaseAdapter {

	private List<Contenido> listContenido;
	private Context ctx;

	public UsersTeloPasoAdapter(Activity activity, List<Contenido> result) {
		this.listContenido = result;
		this.ctx = activity;
	}

	@Override
	public int getCount() {
		return listContenido.size();
	}

	@Override
	public Object getItem(int index) {
		return listContenido.get(index);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.layout_alertas_detalle, parent, false);
		}
		final Contenido contenido = listContenido.get(position);

		TextView txtDescCont = (TextView) v.findViewById(R.id.txtDescContenido);
		TextView txtFeCont = (TextView) v.findViewById(R.id.txtFechaPubl);
		TextView txtPosiCont = (TextView) v.findViewById(R.id.txtPosicion);

		txtDescCont.setText(contenido.getDescCotenido());
		txtFeCont.setText(contenido.getFeContenido());
		StringBuffer sbPosicion = new StringBuffer();
		sbPosicion.append(contenido.getPosicionX()).append("/").append(contenido.getPosicionY());
		txtPosiCont.setText(sbPosicion);
		v.setTag(contenido.getIdContenido());

		return v;
	}
}
