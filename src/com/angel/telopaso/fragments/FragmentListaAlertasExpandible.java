package com.angel.telopaso.fragments;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.angel.telopaso.R;
import com.angel.telopaso.adapters.AlertasAdapter;
import com.angel.telopaso.dao.TeloPasoDaoWrapper;
import com.angel.telopaso.layout.objects.Alerta;
import com.angel.telopaso.layout.objects.Alertas;
import com.angel.telopaso.layout.objects.Contenido;
import com.angel.telopaso.utils.Constantes;

/**
 * Fragmento que se encarga de la gestion de la lista explandible de
 * notificaciones a partir de las alertas
 * */
public class FragmentListaAlertasExpandible extends Fragment {

	private AlertasAdapter alertasAdapter;
	private ListAlertasAsyncLoader alertasAsyncLoader;
	private String userName;
	private List<Alertas> listAlertas = new ArrayList<Alertas>();
	private ExpandableListView listView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View viewPrin = inflater.inflate(R.layout.activity_alertas, container, false);
		SharedPreferences preferences = getActivity().getPreferences(Activity.MODE_PRIVATE);
		userName = preferences.getString("userName", "");
		setHasOptionsMenu(true);

		alertasAsyncLoader = new ListAlertasAsyncLoader();
		alertasAsyncLoader.execute(userName);

		listView = (ExpandableListView) viewPrin.findViewById(R.id.expanAlertas);

		return viewPrin;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		getActivity().getMenuInflater().inflate(R.menu.menu_lista_notificaciones, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		new ListAlertasAsyncLoader().execute(userName);
		return true;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		try {
			Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);

		} catch (NoSuchFieldException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Tarea asincrona que se encarga de carga la lista de de notificaciones aw
	 * partir de las alertas configuradas por el usuario
	 * 
	 * @author angel
	 * */
	class ListAlertasAsyncLoader extends AsyncTask<String, Void, List<Alertas>> {

		private Context ctx = FragmentListaAlertasExpandible.this.getActivity();

		private final ProgressDialog dialog = new ProgressDialog(FragmentListaAlertasExpandible.this.getActivity());

		@Override
		protected void onPostExecute(List<Alertas> result) {
			listAlertas = new ArrayList<Alertas>(result);
			alertasAdapter = new AlertasAdapter(getActivity(), listAlertas);
			listView.setAdapter(alertasAdapter);

			if (dialog != null) {
				dialog.dismiss();
			}

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (dialog != null) {
				dialog.setMessage("Cargando...");
				dialog.show();
			}
		}

		@Override
		protected List<Alertas> doInBackground(String... params) {
			List<Alertas> listSalida = new ArrayList<Alertas>();

			String userName = params[0];
			TeloPasoDaoWrapper daoWrapper = new TeloPasoDaoWrapper(ctx);
			List<Alerta> listAlerta = daoWrapper.getAlertas();
			String entorno = Constantes.ENTORNO_DESA;
			if (!Build.PRODUCT.contains("sdk")) {
				entorno = Constantes.ENTORNO_PRO;
			}

			for (Alerta alerta : listAlerta) {
				try {
					StringBuffer posicion = new StringBuffer();
					posicion.append(alerta.getLongitud()).append(",").append(alerta.getLatitud());
					DefaultHttpClient httpClient = new DefaultHttpClient();

					HttpGet get = new HttpGet("http://" + entorno + "/server/api/send/messages?posicion="
							+ posicion.toString() + "&radio=" + String.valueOf(alerta.getRadio()) + "&userName="
							+ userName);
					HttpResponse response = httpClient.execute(get);
					if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
						Toast.makeText(ctx, "no se ha podido conectar con el servidor", Toast.LENGTH_SHORT).show();
					}

					JSONArray jsonArray = new JSONArray(EntityUtils.toString(response.getEntity()));
					if (jsonArray.length() > 0) {
						List<Contenido> listDatos = new ArrayList<Contenido>();
						for (int i = 0; i < jsonArray.length(); i++) {
							JSONObject jsonObject = jsonArray.getJSONObject(i);
							Contenido contenido = new Contenido();
							contenido.setCategoria(jsonObject.getString(Constantes.CATEGORIA));
							contenido.setDescCotenido(jsonObject.getString(Constantes.DESCRIPCION));
							contenido.setFeContenido(jsonObject.getString(Constantes.FE_MESSAGE));
							contenido.setIdContenido(jsonObject.getString(Constantes.ID_MESSAGE));
							contenido.setTitulo(jsonObject.getString(Constantes.TITULO));
							JSONArray posicionArray = jsonObject.getJSONArray(Constantes.POSICION);
							contenido.setCoordenadas(posicionArray.getString(0) + Constantes.DOS_PUNTOS
									+ posicionArray.getString(1));
							listDatos.add(contenido);
						}

						Alertas alertas = new Alertas();
						alertas.setNomAlerta(alerta.getNombre());
						alertas.setListContenido(listDatos);
						listSalida.add(alertas);
					}
				} catch (IOException e) {
					Log.e(this.getClass().getName(), "Error al intentar acceder al servidor", e);

				} catch (JSONException e) {
					Log.e(this.getClass().getName(), "Error al transformar los datos servidor", e);
				}
			}
			return listSalida;
		}
	}
}