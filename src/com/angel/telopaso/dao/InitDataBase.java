package com.angel.telopaso.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Clase que se encarga de la creacion y actualizacion de la base de datoss
 * */
public class InitDataBase extends SQLiteOpenHelper {

	private static final String CRETAE_TABLE_USER = "CREATE TABLE if not exists USER ( _email TEXT PRIMARY KEY);";
	private static final String CREATE_TABLE_ALERTAS = "CREATE TABLE if not exists ALERTAS (_idAlerta INTEGER PRIMARY KEY AUTOINCREMENT, _nomAlerta TEXT, _posicion TEXT, _radio TEXT, _tagAlerta TEXT);";
	private static final String DROP_TABLE_USER = "DROP TABLE USER";
	private static final String DROP_TABLE_ALERTAS = "DROP TABLE ALERTAS";
	private static final String NAME_DB = "teloPaso";
	private static final int VERSION_DB = 1;

	public InitDataBase(Context context) {
		super(context, NAME_DB, null, VERSION_DB);

	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CRETAE_TABLE_USER);
		db.execSQL(CREATE_TABLE_ALERTAS);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
		db.execSQL(DROP_TABLE_USER);
		db.execSQL(DROP_TABLE_ALERTAS);
		onCreate(db);

	}
}
