package com.angel.telopaso.layout.objects;

/**
 * POJO que contiene los datos para el registro de un usuario
 * */
public class User {

	private String emailUser;

	public String getEmailUser() {
		return emailUser;
	}

	public void setEmailUser(String emailUser) {
		this.emailUser = emailUser;
	}

}
